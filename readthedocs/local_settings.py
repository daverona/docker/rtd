# -*- coding: utf-8 -*-

# References
# https://docs.readthedocs.io/en/stable/custom_installs/customization.html
# https://docs.readthedocs.io/en/stable/settings.html
# https://docs.djangoproject.com/en/1.11/ref/settings/
# https://github.com/readthedocs/readthedocs.org/tree/3.4.2/readthedocs/settings

import os


def listconv(string, sep=','):
    return map(str.strip, string.split(sep))


def boolconv(string):
    return string.lower().strip() in ['true', 'yes', 'on']


DEBUG = False
TIME_ZONE = os.getenv('TZ', 'UTC')
LANGUAGE_CODE = os.getenv('RTD_LANGUAGE_CODE', 'en-us')
SECRET_KEY = os.getenv('RTD_SECRET_KEY', 'unset')

###############################################################################
# authentication backends
# @see https://docs.djangoproject.com/en/1.11/topics/auth/customizing/#specifying-authentication-backends  # noqa
# @see https://django-guardian.readthedocs.io/en/stable/configuration.html
###############################################################################
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    "allauth.account.auth_backends.AuthenticationBackend",
    'guardian.backends.ObjectPermissionBackend',
)

###############################################################################
# domains and urls (base.py, dev.py)
# https://docs.readthedocs.io/en/stable/development/settings.html#production-domain  # noqa
###############################################################################
PUBLIC_DOMAIN = os.getenv('RTD_SITE_DOMAIN', 'localhost')
_protocol = os.getenv('RTD_DOMAIN_PROTOCOL', 'http').lower()
PUBLIC_DOMAIN_USES_HTTPS = _protocol == 'https'
USE_SUBDOMAIN = boolconv(os.getenv('RTD_SITE_SUBDOMAIN_USE', 'False'))
PRODUCTION_DOMAIN = os.getenv('RTD_PRODUCTION_DOMAIN', 'localhost')
PUBLIC_API_URL = '{0}://{1}'.format(_protocol, os.getenv('RTD_API_DOMAIN', 'localhost'))  # noqa

###############################################################################
# slumber (dev.py)
###############################################################################
SLUMBER_USERNAME = os.getenv('RTD_SLUMBER_USERNAME', 'slumber')
SLUMBER_PASSWORD = os.getenv('RTD_SLUMBER_PASSWORD', 'secret')
SLUMBER_API_HOST = '{0}://{1}'.format(_protocol, os.getenv('RTD_SLUMBER_HOSTNAME', 'localhost'))  # noqa

###############################################################################
# admins and managers
# @see https://docs.djangoproject.com/en/1.11/ref/settings/#admins
# @see https://docs.djangoproject.com/en/1.11/ref/settings/#managers
# @see https://docs.djangoproject.com/en/1.11/ref/settings/#std:setting-SERVER_EMAIL  # noqa
# @see https://docs.djangoproject.com/en/1.11/ref/settings/#email-subject-prefix
###############################################################################
ADMINS = [(email, email) for email in listconv(os.getenv('RTD_ADMIN_EMAILS', 'admin@localhost'))]  # noqa
MANAGERS = [(email, email) for email in listconv(os.getenv('RTD_MANAGER_EMAILS', 'manager@localhost'))]  # noqa

###############################################################################
# accounts
# @see https://django-allauth.readthedocs.io/en/latest/configuration.html
###############################################################################
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_LOGIN_ATTEMPTS_LIMIT = 5
ACCOUNT_LOGIN_ATTEMPTS_TIMEOUT = 300  # seconds
ACCOUNT_EMAIL_VERIFICATION = os.getenv('RTD_EMAIL_VERIFICATION', 'none')  # mandatory, optional, or none  # noqa
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 3
ACCOUNT_LOGOUT_ON_PASSWORD_CHANGE = True
ACCOUNT_PRESERVE_USERNAME_CASING = False
ACCOUNT_USERNAME_MIN_LENGTH = 5
OLD_PASSWORD_FIELD_ENABLED = False
LOGOUT_OLD_PASSWORD_CHANGE = True
ACCOUNT_LOGOUT_REDIRECT_URL = '/'
LOGIN_REDIRECT_URL = '/'

# ACCOUNT_EMAIL_SUBJECT_PREFIX is used when to send to users
ACCOUNT_EMAIL_SUBJECT_PREFIX = os.getenv('RTD_DEFAULT_EMAIL_SUBJECT_PREFIX', '[RTD]') + ' '  # noqa
# EMAIL_SUBJECT_PREFIX is used when to send to ADMINS and MANAGERS
EMAIL_SUBJECT_PREFIX = os.getenv('RTD_SERVER_EMAIL_SUBJECT_PREFIX', '[RTD]') + ' '  # noqa

###############################################################################
# private repositories (base.py)
###############################################################################
ALLOW_PRIVATE_REPOS = True

###############################################################################
# email
# @see https://docs.djangoproject.com/en/1.11/topics/email/
# @see https://docs.djangoproject.com/en/1.11/ref/settings/
###############################################################################
if os.getenv('RTD_MAIL_BACKEND', 'console').lower().strip() == 'smtp':
    EMAIL_BACKEND = os.getenv('RTD_SMTP_BACKEND', 'django.core.mail.backends.smtp.EmailBackend')  # noqa
    EMAIL_HOST = os.getenv('RTD_SMTP_HOSTNAME', 'smtp.gmail.com')
    EMAIL_PORT = os.getenv('RTD_STMP_PORT', 587)
    EMAIL_HOST_USER = os.getenv('RTD_SMTP_USERNAME', '')
    EMAIL_HOST_PASSWORD = os.getenv('RTD_SMTP_PASSWORD', '')
    _encrypt = os.getenv('RTD_SMTP_ENCRYPT', 'tls').lower().strip()
    EMAIL_USE_SSL = _encrypt == 'ssl'
    EMAIL_USE_TLS = _encrypt == 'tls'

    # DEFAULT_FROM_MAIL is used for various automated correspondence to users
    DEFAULT_FROM_EMAIL = os.getenv('RTD_DEFAULT_EMAIL', EMAIL_HOST_USER)
    # SERVER_EMAIL is used to send to ADMINS and MANAGERS error messages
    SERVER_EMAIL = os.getenv('RTD_SERVER_EMAIL', DEFAULT_FROM_EMAIL)

# Noate that if RTD_SMTP_BACKEND is 'auxes.smtp_backend.BccEmailBackend' 
# EMAIL_HOST_USER will receive all sent mails as bcc.  This is useful if your 
# SMTP service provider does not keep sent mails or does not show them.

###############################################################################
# database (dev.py)
# @see https://docs.djangoproject.com/en/1.11/ref/settings/#databases
###############################################################################
if os.getenv('RTD_DB_BACKEND', 'sqlite3').lower().strip() == 'mysql':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'HOST': os.getenv('RTD_MYSQL_HOSTNAME', 'localhost'),
            'USER': os.getenv('RTD_MYSQL_USERNAME', 'rtd'),
            'PASSWORD': os.getenv('RTD_MYSQL_PASSWORD', 'secret'),
            'NAME': os.getenv('RTD_MYSQL_DATABASE', 'rtd'),
            'PORT': os.getenv('RTD_MYSQL_PORT', 3306),
        }
    }

###############################################################################
# cache (base.py, dev.py)
# @see https://niwinz.github.io/django-redis/latest/
###############################################################################
if os.getenv('RTD_CACHE_BACKEND', 'redis').lower().strip() == 'redis':
    CACHES = {
        'default': {
            'BACKEND': 'django_redis.cache.RedisCache',
            'LOCATION': os.getenv('RTD_REDIS_LOCATION', 'redis://localhost:6379/1'),  # noqa
            'OPTIONS': {
                'CLIENT_CLASS': 'django_redis.client.DefaultClient',
                'PASSWORD': os.getenv('RTD_REDIS_PASSWORD', None),
            }
        }
    }

###############################################################################
# celery (dev.py)
###############################################################################
BROKER_URL = os.getenv('RTD_BROKER_URL', 'redis://localhost:6379/0')
CELERY_RESULT_BACKEND = os.getenv('RTD_CELERY_RESULT_BACKEND', BROKER_URL)

###############################################################################
# elasticsearch (base.py, dev.py)
# @see https://github.com/django-es/django-elasticsearch-dsl
###############################################################################
ES_HOSTS = [host for host in listconv(os.getenv('RTD_ES_HOSTS', 'localhost:9200'))]  # noqa
ELASTICSEARCH_DSL = {
    'default': { 'hosts': os.getenv('RTD_ES_DSL_HOSTS', 'localhost:9200'), },
}
ELASTICSEARCH_DSL_AUTOSYNC = True

###############################################################################
# production security
###############################################################################
# (security.W002)
#MIDDLEWARE += ['django.middleware.clickjacking.XFrameOptionsMiddleware', ]
#X_FRAME_OPTIONS = 'DENY'
# (security.W004)
#SECURE_HSTS_INCLUDE_SUBDOMAINS = True
#SECURE_HSTS_PRELOAD = True
#SECURE_HSTS_SECONDS = 15768000
# (security.W006)
#SECURE_CONTENT_TYPE_NOSNIFF = True
# (security.W007) 
#SECURE_BROWSER_XSS_FILTER = True
# (security.W008) 
#SECURE_SSL_REDIRECT = True
# (security.W010) 
#SESSION_COOKIE_SECURE = True
# (security.W016) 
#CSRF_COOKIE_SECURE = True
#CSRF_COOKIE_HTTPONLY = True
