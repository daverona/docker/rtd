from django.conf import settings
from django.core.mail.backends.smtp import EmailBackend


class BccEmailBackend(EmailBackend):
    def _send(self, email_message):
        """
        This is a quick and dirty heck for the problem with SMTP servers in
        Daou Office email service.

        Daou Office web mail does not show (and possibly does not keep)
        the emails which has been sent out not via its web interface.
        In other words, emails sent by Django EmailBackend won't show up in
        the sent box in Daou Office web mail. (The delivery is okay though.)
        Which means, you don't have any record about sent-out emails.

        This function bcces settings.EMAIL_HOST_USER to all outgoing emails
        so that the emails can be kept in Daou Office inbox (instead of
        sent box) for future reference. (The original recipients won't know
        about this because it's bcc, not to nor cc.)
        """
        if not email_message.bcc:
            email_message.bcc = [
                settings.EMAIL_HOST_USER,
            ]
        return super(BccEmailBackend, self)._send(email_message)
