FROM alpine:3.9

ENV LANG=C.UTF-8

RUN apk add --no-cache \
    elasticsearch \
    logrotate \
    nginx \
    nginx-mod-http-headers-more \
    python3 \
    redis \
    supervisor \
    texlive \
    #texlive-luatex \
    #texlive-xetex \
    texmf-dist \
    texmf-dist-formatsextra \
    texmf-dist-latexextra \
    texmf-dist-pictures \
    texmf-dist-science \
    #texmf-dist-langkorean \
    tzdata \
  && python3 -m pip install --no-cache-dir --upgrade pip \
  && pip3 install --no-cache-dir setuptools wheel \
  # Configure redis
  && mkdir -p /var/log/redis /var/run/redis \
  && chown -R redis:redis /var/log/redis /var/run/redis \
  # Configure elasticsearch
  # @see https://www.elastic.co/guide/en/elasticsearch/reference/6.0/settings.html
  # @see https://stackoverflow.com/questions/40766301/run-elastic-search-as-root-user
  && mkdir -p /var/lib/elasticsearch/_default/plugins /var/log/elasticsearch \
  && chown -R elastico:elastico /var/lib/elasticsearch /var/log/elasticsearch \
  && sed -i -r -e "s|#?(path.data:)\s.*|\1 /var/lib/elasticsearch|" \
    -e "s|#?(path.logs:)\s.*|\1 /var/log/elasticsearch|" /etc/elasticsearch/elasticsearch.yml \
  && sed -i "s|logs/|/var/log/elasticsearch/|" /etc/elasticsearch/jvm.options \
  # Install elasticsearch analysis plugins for Korean
  # @see https://www.elastic.co/guide/en/elasticsearch/plugins/6.0/analysis-icu.html
  # @see https://www.elastic.co/guide/en/elasticsearch/plugins/6.4/analysis-nori.html
  #&& ES_PATH_CONF=/etc/elasticsearch /usr/share/java/elasticsearch/bin/elasticsearch-plugin install analysis-nori \
  #&& ES_PATH_CONF=/etc/elasticsearch /usr/share/java/elasticsearch/bin/elasticsearch-plugin install analysis-icu \
  #&& chown -R elastico:elastico /var/lib/elasticsearch \
  # Configure nginx
  && mkdir -p /var/run/nginx \
  # Configure supervisor
  && mkdir -p /var/log/supervisor

ARG RTD_VERSION=3.4.2

# Install Read the Docs
RUN true \
  # Install runtime dependencies
  && apk add --no-cache \
    build-base \
    git \
    jpeg-dev \
    libxml2-dev \
    libxslt-dev \
    mariadb-connector-c \
    mercurial \
    postgresql-libs \
    python3-dev \
  # Install build-time dependencies
  && apk add --no-cache --virtual=build-deps \
    mariadb-connector-c-dev \
    postgresql-dev \
  && mkdir /app \
  && wget -qO- --no-check-certificate "https://github.com/readthedocs/readthedocs.org/archive/${RTD_VERSION}.tar.gz" \
    | tar -zxvf - -C /app --strip-components=1 \
  # Delete unnecessary files and directories for clean exposure
  && cd /app && rm -rf bower.json common contrib docs conftest.py gulpfile.js \
    MANIFEST.in package.json prospector*.yml pytest.ini scripts tasks.py tox.ini && cd - \
  && (rm -rf /app/.* > /dev/null 2>&1 || true) \
  # Specify packages to install
  && sed -i -r "s|^(django-debug-toolbar=.*)$|#\1|" /app/requirements/pip.txt \
  # PROBLEM: ModuleNotFoundError: No module named 'gitdb.utils.compat'
  # SOLUTION: https://github.com/NervanaSystems/distiller/issues/475#issuecomment-589938449
  && echo "gitdb2==2.0.6" >> /app/requirements/pip.txt \
  # PROBLEM: kombu.exceptions.VersionMismatch: Redis transport requires redis-py versions 3.2.0 or later. You have 2.10.6
  # SOLUTION: https://github.com/pydanny/cookiecutter-django/issues/1954#issue-418815563
  && echo "kombu==4.3.0" >> /app/requirements/pip.txt \
  && echo "mysqlclient==2.0.1" >> /app/requirements/deploy.txt \
  # Comment out all lines including "debug_toolbar"
  && sed -i -r "s|^(.*debug_toolbar.*)$|#\1|" /app/readthedocs/urls.py \
  && sed -i -r "s|^(.*debug_toolbar.*)$|#\1|" /app/readthedocs/settings/dev.py \
  # PROBLEM: FileNotFoundError: [Errno 2] No such file or directory: 'python3.7': 'python3.7'
  # SOLUTION: https://github.com/rtfd/readthedocs.org/issues/5335
  && sed -i -r "s|^(.*), 3\.7(.*)$|\1\2|" /app/readthedocs/settings/base.py \
  # Install RTD packages
  && pip3 install --no-cache-dir --requirement /app/requirements/deploy.txt \
  && apk del --no-cache build-deps \
  # Configure celery
  && mkdir -p /var/run/celery /var/log/celery \
  # Configure gunicorn
  && mkdir -p /var/log/gunicorn \
  && chmod a+x /app/manage.py \
  # Do makemigrations (for just in case)
  && python3 /app/manage.py makemigrations \
  # Collect static files
  && python3 /app/manage.py collectstatic --no-input \
  && rm -rf /app/dev.db /app/logs/*

SHELL ["/bin/bash", "-c"]
RUN { \
    # Make and symlink data directories for easy back-up
    mkdir -p /data; \
    data_paths=(cnames private_cname_project private_cname_root private_web_root prod_artifacts public_cname_project public_cname_root public_web_root user_builds user_uploads); \
    for path in "${data_paths[@]}"; do \
      mkdir -p "/data/$path"; \
      ln -sf "/data/$path" "/app/$path"; \
    done; \
  }
SHELL ["/bin/sh", "-c"]

# Configure miscellanea
COPY logrotate/ /etc/logrotate.d/
COPY readthedocs/local_settings.py /app/readthedocs/settings/local_settings.py
COPY readthedocs/auxes /app/readthedocs/auxes
COPY nginx/ /etc/nginx/conf.d/
COPY supervisor/ /etc/supervisor.d/
COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod a+x /docker-entrypoint.sh
EXPOSE 80/tcp 443/tcp
WORKDIR /app

ENV RTD_VERSION=$RTD_VERSION

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf", "-n"]
