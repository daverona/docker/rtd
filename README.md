# daverona/rtd

[![pipeline status](https://gitlab.com/daverona/docker/rtd/badges/master/pipeline.svg)](https://gitlab.com/daverona/docker/rtd/commits/master)

This is a repository for Docker images of [Read the Docs](https://docs.readthedocs.io/).

* GitLab repository: [https://gitlab.com/daverona/docker/rtd](https://gitlab.com/daverona/docker/rtd)
* Docker registry: [https://hub.docker.com/r/daverona/rtd](https://hub.docker.com/r/daverona/rtd)
* Available releases: [https://gitlab.com/daverona/docker/rtd/-/releases](https://gitlab.com/daverona/docker/rtd/-/releases)

## Quick Start

Run a container:

```bash
docker container run --rm \
  --detach \
  --publish 80:80 \
  --name rtd \
  daverona/rtd
```

Visit [http://localhost](http://localhos) and verify that Read the
Docs works. To do so, sign up and import the demo project to see if the demo
documentation builds correctly. Once it has been built, browse around the result
documentation.

```bash
python3 /app/manage.py migrate
python3 /app/manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@localhost', 'secret')"
python3 /app/manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('slumber', 'slumber@localhost', 'secret')"
```

Then log in to the administration site
([http://localhost:8000/admin](http://localhost:8000/admin)) to make sure you
can log in.
The default admin username and password are *admin* and *secret* respectively.
You must change the admin's password immediately once you log in to the admin
site.

## Customization

In this section we explain how to customize Read the Docs server.

There are documentations available for helping customization:

* [Read the Docs settings](https://docs.readthedocs.io/en/stable/development/settings.html)
* [Django settings](https://docs.djangoproject.com/en/2.2/ref/settings/)
(Django is the Python framework on which Read the Docs is built.)

There are three ways to apply the customization:

1. Pass the environment variables with customized values to the container when
the container starts.
2. Modify Read the Docs (and/or Django) variables in local_settings.py file and
replace the one in the container with it by mounting the file as a volume.
local_settings.py in the container is located at
/readthedocs.org/readthedocs/settings
and local_settings.py used for image build is located at
./readthedocs/local_settings.py in the git repository.
3. Same as the second but this time build the image with the customized
local_settings.py. All you need to do is modify ./readthedocs/local_settings.py
in the git repository and build the image.

It sounds complicated but it's really not.

To pass an environment variable, say `ENV_VAR`, to the container you can do this:

```bash
docker container run --rm \
  --detach \
  --publish 8000:8000 \
  --env ENV_VAR=value \
  --name rtd \
  daverona/rtd
```

To replace local_settings.py in the container:

```bash
docker container run --rm \
  --detach \
  --publish 8000:8000 \
  --volume /host/path/to/local_settings.py:/readthedocs.org/readthedocs/settings/local_settings.py \
  --name rtd \
  daverona/rtd
```

Building the image is described in the installation section above.

We will explain environment variables that can change Read the Docs server
configuration below. Remember, you can modify the values of Read the Docs
variables in local_settings.py corresponding to these environment variables.

### Time Zone and Secret

* `RTD_TIMEZONE`: the time zone of Read the Docs server (default: 'UTC')
* `RTD_SECRET_KEY`: a key used to secure the data (default: 'replace-this-please')

You really should set `RTD_SECRET_KEY`. And you certainly do *not* want to store
this value to local_settings.py for security reason. If you don't know how to
generate a random string for the secret key, here is a website that makes one:
[https://djecrety.ir/](https://djecrety.ir/).

### Database

By default the database used by Read the Docs server is SQLite
(see [https://www.sqlite.org/index.html](https://www.sqlite.org/index.html))
and SQLite database file is located at /readthedocs.org/dev.db. If you like to
use to MariaDB (see [https://mariadb.org/](https://mariadb.org/)) or MySQL
(see [https://www.mysql.com/](https://www.mysql.com/)), you need to pass the
following environment variables to the container:

* `DB_DRIVER`: 'mysql' (default: 'sqlite3')
* `MYSQL_HOSTNAME`: IP address of MariaDB (or MySQL) server (default: 'localhost')
* `MYSQL_USERNAME`: username to use for the DBMS server (default: 'rtd')
* `MYSQL_PASSWORD`: password to use for the DBMS server (default: 'secret')
* `MYSQL_DATABASE`: database to use for the DBMS server (default: 'rtd')
* `MYSQL_PORT`: port to use for the DBMS server (default: 3306)

For other types of DBMSes, please refer to
[https://docs.djangoproject.com/en/2.2/ref/databases/](https://docs.djangoproject.com/en/2.2/ref/databases/).

### Migration

Before switching to MariaDB, back up the SQLite database as a JSON fixture while
the container is running and then stop the container:

```bash
docker container exec \
  --interactive --tty \
  rtd sh -c "python manage.py dumpdata > /tmp/dump.json"
docker cp rtd:/tmp/dump.json .
docker container stop rtd
```

Before the container with MariaDB starts, make sure your MariaDB is reachable
from the container and a MariaDB database and a user account which can access
the database have been created for Read the Docs server:

```sql
DROP DATABASE IF EXISTS rtd;
CREATE DATABASE rtd DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'rtd'@'%';
SET PASSWORD FOR 'rtd'@'%' = PASSWORD('secret');
GRANT ALL PRIVILEGES ON rtd.* TO 'rtd'@'%';
FLUSH PRIVILEGES;
```

Start a new container with environment variables explained above and make the
JSON fixture available:

```bash
docker container run --rm \
  --detach \
  --env DB_DRIVER=mysql \
  --env MYSQL_HOSTNAME=mariadb \
  --env MYSQL_USERNAME=rtd \
  --env MYSQL_PASSWORD=secret \
  --env MYSQL_DATABASE=rtd \
  --publish 8000:8000 \
  --name rtd \
  daverona/rtd
docker cp dump.json rtd:/tmp
```

Finally migrate to MariaDB using the JSON fixture in the container:

```bash
docker container exec --interactive --tty rtd sh
python manage.py migrate --run-syncdb
python manage.py shell -c "from django.contrib.contenttypes.models import ContentType; \
  ContentType.objects.all().delete()"
python manage.py loaddata /tmp/dump.json
exit
```

<!-- apk update &&\
apk add python3 python3-dev mariadb-dev build-base &&\
pip3 install mysqlclient &&\
apk del python3-dev mariadb-dev build-base &&\
apk add mariadb-client-libs -->

### Email

Read the Docs server can send emails to a user to verify the email address,
reset the user password and notify failure during the documentation build.
By default this feature is disabled. You need to set up an SMTP account to
enable this feature by setting the following variables:

* `EMAIL_DRIVER`: 'smtp' (default: 'console')
* `EMAIL_HOSTNAME`: IP address of the SMTP server to use for sending email (default: 'smtp.google.com')
* `EMAIL_PORT`: port to use for the SMTP server (default: 587)
* `EMAIL_USE_SSL`: whether to use an implicit TLS connection to the SMTP server (default: False)
* `EMAIL_USE_TLS`: whether to use a TLS connection to the SMTP server (default: True)
* `EMAIL_USERNAME`: username to use for the SMTP server (default: '')
* `EMAIL_PASSWORD`: password to use for the SMTP server (default: '')
* `EMAIL_DEFAULT_EMAIL`: email address for automated correspondence from the site managers (default: 'webmaster@localhost')
* `EMAIL_SERVER_EMAIL`: email address for error messages (default: 'root@localhost')

If you use a Google gmail account in this setup and this account is never used
by application servers such as Read the Docs, Google will send for security
to the account a verification email if the activity is yours.
Until you confirm, Google SMTP server blocks all requests originated from the
application server. So after the setup nothing seems to happen, please check
your mail box looking for a verification email from Google.

For more information please read [https://docs.djangoproject.com/en/2.2/ref/settings/#email-backend](https://docs.djangoproject.com/en/1.11/ref/settings/#email-backend).

### Admins and Managers

An administrator of Read the Docs server can receive code error notifications
and a manager can receive broken link notifications. Once you have set up
an SMTP account, set these variables to receive notifications:

* `RTD_ADMIN_EMAILS`: whitespace-separated administrator email addresses (default: 'admin@localhost')
* `RTD_MANAGER_EMAILS`: whitespace-separated manager email addresses (default: 'manager@localhost')

For more information please read
[https://docs.djangoproject.com/en/2.2/ref/settings/#admins](https://docs.djangoproject.com/en/2.2/ref/settings/#admins)
and
[https://docs.djangoproject.com/en/2.2/ref/settings/#managers](https://docs.djangoproject.com/en/2.2/ref/settings/#managers).

### SSL Connection

Write this section.

### Redis

If you like to use a Redis server other than the one included in the container:
* `REDIS_HOSTNAME`: IP address of Redis server (default: 'localhost')
* `REDIS_PORT`: port to use for Redis server (default: 6379)
* `REDIS_DATABASE`: database to use for Redis server (default: 0)

### Elasticsearch

If you like to use an Elasticsearch server other than the one included in the container:

* `ELASTICSEARCH_HOSTNAME`: IP address of Elasticsearch server (default: 'localhost')
* `ELASTICSEARCH_PORT`: port to use for Elasticsearch server (default: 9200)

## How-Tos

### Private Repository in GitLab

Create a personal access token in GitLab. Please refer to
[https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
if you don't know how to create it. Come back to your Read the Docs server,
go to your dashboard, click <ins>Import a Project</ins> button and then click
<ins>Import Manually</ins> button. In <ins>Repository URL</ins> type:

```
https://<gitlab-login>:<personal-access-token>@gitlab.com/<namespace>/<private-repo>.git
```

### Private Repository in GitHub

Create a personal access token in GitHub. Please refer to
[https://help.github.com/en/articles/creating-a-personal-access-token-for-the-command-line](https://help.github.com/en/articles/creating-a-personal-access-token-for-the-command-line)
if you don't know how to create it. Come back to your Read the Docs server,
go to your dashboard, click <ins>Import a Project</ins> button and then click
<ins>Import Manually</ins> button. In <ins>Repository URL</ins> type:

```
https://<personal-access-token>:x-oauth-basic@github.com/<namespace>/<private-repo>.git
```

### Webhooks

Please refer to [https://docs.readthedocs.io/en/stable/webhooks.html#webhook-creation](https://docs.readthedocs.io/en/stable/webhooks.html#webhook-creation) to create a webhook for GitLab, GitHub and/or Bitbucket.

## References

* [https://docs.readthedocs.io/en/stable/index.html](https://docs.readthedocs.io/en/stable/index.html)
* [https://docs.readthedocs.io/en/stable/development/settings.html](https://docs.readthedocs.io/en/stable/development/settings.html)
* [https://docs.djangoproject.com/en/2.2/ref/settings/](https://docs.djangoproject.com/en/2.2/ref/settings/)
