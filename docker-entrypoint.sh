#!/bin/bash
set -e

color_red='\033[1;31m'
color_yellow='\033[1;33m'
color_cyan='\033[1;36m'
color_white_bold='\033[1;97m'
color_reset='\033[0m'

if [ "supervisord" == "$(basename $1)" ]; then
  # Check if SECRET_KEY is provided
  secret_key=$(python3 /app/manage.py shell -c "from django.conf import settings;print(settings.SECRET_KEY)" 2> /dev/null || echo "unset")
  if [ "${secret_key}" == "unset" ]; then
    echo
    echo -e "${color_red}ERROR: cannot proceed because environment variable RTD_SECRET_KEY is not set.${color_reset}"
    secret_key=$(python3 /app/manage.py shell -c "from django.core.management.utils import get_random_secret_key;print(get_random_secret_key())")
    echo -e "You may use this: ${color_white_bold}${secret_key}${color_reset}"
    echo
    exit 1
  fi

  # @see https://docs.gunicorn.org/en/stable/design.html#how-many-workers
  numofworkers=$(($(nproc) * 2 + 1))
  export WEB_CONCURRENCY=${WEB_CONCURRENCY:-${numofworkers}}

  # PROBLEM: RuntimeWarning: You're running the worker with superuser privileges: this is absolutely not recommended!
  # SOLUTION: https://docs.celeryproject.org/en/stable/userguide/daemonizing.html?highlight=C_FORCE_ROOT#running-the-worker-with-superuser-privileges-root
  export C_FORCE_ROOT="true"  # this is not working...

  # Check if "python3 manage.py migrate" is ever executed
  ever_migrated=$(python3 /app/manage.py showmigrations --plan | cut -c2 | uniq | sort)
  if [ ${#ever_migrated[@]} -eq 1 ] && [ "${ever_migrated[0]}" == " " ]; then
    printf "${color_yellow}WARNING: the database is never synced with the models and migrations.${color_reset}"
    echo -e " (run: ${color_white_bold}python3 /app/manage.py migrate${color_reset})"
  fi

  # Check if superuser "admin" and "slumber" exist
  numofsuperusers=$(python3 /app/manage.py shell -c "from django.contrib.auth.models import User;print(User.objects.filter(is_superuser=True).count())" 2> /dev/null || echo 0)
  if [ "2" -gt "${numofsuperusers}" ]; then
    printf "${color_yellow}WARNING: the superusers 'admin' and 'slumber' do not exist.${color_reset}"
    echo -e " (run: ${color_white_bold}python3 /app/manage.py createsuperuser${color_reset})"
  fi

  # Change ownership and permission for data directories
  printf "${color_cyan}INFO: checking the ownership and the permission of data directories...${color_reset}"
  data_paths=(cnames private_cname_project private_cname_root private_web_root prod_artifacts public_cname_project public_cname_root public_web_root user_builds user_uploads)
  for path in "${data_paths[@]}"; do
    mkdir -p "/data/$path"
    ln -sf "/data/$path" "/app/$path"
    find /data/. -type d -exec chown root:nginx {} \; -exec chmod 750 {} \;
  done
  echo " done"
  echo
fi

exec "$@"
