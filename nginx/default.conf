upstream django_server {
    server 127.0.0.1:8000;
}

server {
  location / {
    proxy_pass http://django_server;
    proxy_redirect off;
    # We require the correct Host to be set so that subdomains work
    proxy_set_header Host $host;
    # TODO: Use X-Real-IP if already set
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_read_timeout 300;
  }

  location /public_web_root/ { internal; alias /app/public_web_root/; }

  # The magic happens here. RTD backend replies with a X-Accel-Redirect
  # header to instruct nginx to serve a given static file.
  # The "internal" directive handles that.
  # The location seems prefixed with /user_builds so we rely on that.

  location /user_builds/ { internal; alias /app/user_builds/; }

  location /static/ { alias /app/static/; access_log off; log_not_found off; }
  location /media/ { alias /app/media/; }
  location = /favicon.ico { alias /app/static/images/favicon.ico; access_log off; log_not_found off; }
  location = /robots.txt { alias /app/static/robots.txt; access_log off; log_not_found off; }

  include /etc/nginx/conf.d/default/*.conf;
}
